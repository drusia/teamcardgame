package CardGame;

import java.util.ArrayList;

import CardGame.Card;
import CardGame.Player;
import CardGame.Suit;

public class Player {
	
	protected ArrayList<Card> cards = new ArrayList<Card>();
	protected double score;
	
	public Player(Suit s)	{
		score = 0;
		for (int i = 1; i <= 13; i++)	{
			cards.add(new Card(i, s));
		}
	}
	
	public double getScore() {
		return score;
	}
	
	private void setScore(double score) {
		this.score = score;
	}

	public void updateScore(double pointsWon)	{
		this.setScore(this.getScore() + pointsWon);
	}
	
	public void displayAvailableCards()	{
		for (int i = 0; i < cards.size(); i++)	{
			System.out.println(cards.get(i).getFaceValue());
		}
	}
	
	public boolean isCardPresent(Card C)	{
		for (int i = 0; i < cards.size(); i++)	{
			if (cards.get(i).equals(C))	return true;
		}
		return false;
	}
	
	public static void main(String[] args)	{
		Player ply = new Player(Suit.CLUB);
		ply.displayAvailableCards();
	}
}
