package CardGame;

public enum Suit {

	HEART,
	SPADE,
	CLUB,
	DIAMOND
}
