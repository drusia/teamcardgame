package teamcardgame;

public class Human extends Player	{

	public Human(Suit s) {
		super(s);
	}
	
	public Card playCard(Card aucCard, Card cToPlay)	{
		this.cards.remove(cToPlay);
		return cToPlay;
	}
}
