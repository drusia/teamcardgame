package teamcardgame;

import java.util.Random;

public class Computer extends Player {

	public Computer(Suit s) {
		super(s);
	}
	
	Card playCard()	{
		Random ran = new Random();
		int index = ran.nextInt(this.cards.size());
		Card c = this.cards.get(index) ;
		this.cards.remove(index);
		return c;
	}

}
