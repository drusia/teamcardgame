package paypalbootcamp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

class Game
{
	public ArrayList<Integer> suffle()
	{
		ArrayList<Integer> initialcards= new ArrayList<Integer>() ;
		ArrayList<Integer> finalcards= new ArrayList<Integer>() ;
		for (int i = 0 ; i < 13 ; i++ )
			initialcards.add(i+1);
		Random rn = new Random();
		for (int i = 0 ; i < 13 ; i ++)
		{
			int randomNumber = (int) ( rn.nextInt(initialcards.size()));
			finalcards.add(initialcards.get(randomNumber));
			initialcards.remove(randomNumber);
		}	
		return 	finalcards;
		
	}
}

public class CardGame
{
	public static void main( String args[]) throws IOException
	{
		Game G= new Game();
		System.out.print(G.suffle());
	}
}
